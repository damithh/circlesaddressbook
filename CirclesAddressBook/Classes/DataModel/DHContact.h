//
//  DHContact.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


typedef enum {
    ContactChangeModeNotChaged,
    ContactChangeModeAdded,
    ContactChangeModeDeleted,
    ContactChangeModeUpdated
} ContactChangeMode;

@interface DHContact : NSManagedObject

@property (nonatomic, retain) NSString * contactName;
@property (nonatomic, retain) NSNumber * contactId;
@property (nonatomic, retain) NSString * contactPhoneNumber;
@property (nonatomic, retain) NSNumber * contactChangeMode;

@end
