//
//  DHContact.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "DHContact.h"


@implementation DHContact

@dynamic contactName;
@dynamic contactId;
@dynamic contactPhoneNumber;
@dynamic contactChangeMode;

@end
