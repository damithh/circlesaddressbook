//
//  DHContactsTVC.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHContact.h"

typedef enum {
    DHContactCellTypeAll,
    DHContactCellTypeChanged
} DHContactCellType;


@interface DHContactsTVC : UITableViewCell

@property(nonatomic,assign)DHContactCellType cellType;
@property(nonatomic,strong)DHContact *contact;

@end
