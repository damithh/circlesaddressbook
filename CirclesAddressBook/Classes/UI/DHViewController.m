//
//  DHViewController.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/7/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "DHViewController.h"
#import "DHActivityIndicator.h"
#import "DHAppDelegate.h" 
#import "ABConnector.h"
#import "DHContactsTVC.h"

static const NSString *strTapSyncButton=@"Tap \"Sync\" button to sync contacts";
static const NSString *strTapShowChanges=@"Sync is done.Tap \"Show Changes\" to show Changes";
static const NSString *strSyncing=@"Syncing in porogress...";


@interface DHViewController ()<UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbltTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnToggle;
@property (weak, nonatomic) IBOutlet UIButton *btnSync;
@property (strong, nonatomic) NSMutableArray *dataArray;



- (IBAction)btnToggleTap:(id)sender;
- (IBAction)btnSyncTap:(UIButton *)sender;
@end

@implementation DHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_lbltTitle setText:@"Tap \"Sync\" button to sync contacts"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncCompleted:) name:SyncCompletedNotification object:nil];
    if (!_dataArray) {
        self.dataArray = [[NSMutableArray alloc]initWithArray:[[DBManager sharedInstance]getAllContacts]];
    }
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSyncTap:(UIButton *)sender {
    [_lbltTitle setText:@"Syncing in porogress..."];
    [[ABConnector sharedInstance]syncWithAddressBook];
}


- (IBAction)btnToggleTap:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    [_dataArray removeAllObjects];
    if (btn.selected) {
//     show alll
        if ([[[DBManager sharedInstance]getModifiedContacts]count]>0) {
            [_lbltTitle setText:@"Sync is done.Tap \"Show Changes\" to show Changes"]; 
        }else{
            [_lbltTitle setText:@"Sync is done.No Changes to Show"]; 
        } 
        [_dataArray addObjectsFromArray:[[DBManager sharedInstance]getAllContacts]];
    }else{
//    show changes
        [_lbltTitle setText:@"Sync is done.Tap \"Show All\" to show all contacts"];
        [_dataArray addObjectsFromArray:[[DBManager sharedInstance]getModifiedContacts]];
    }
    [_tableView reloadData];
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    DHContactsTVC *cell = (DHContactsTVC *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell =[[DHContactsTVC alloc]init];
        [cell setSelectionStyle:UITableViewCellEditingStyleNone];
	}
    DHContact *contact = [_dataArray objectAtIndex:indexPath.row];
    if (_btnToggle.isHidden || _btnToggle.selected) {
        [cell setCellType:DHContactCellTypeAll];
    }else{
        [cell setCellType:DHContactCellTypeChanged];
    }
    [cell setContact:contact]; 
    return cell; 
}

- (void)syncCompleted:(NSNotification *)notifi{
    if ([[[DBManager sharedInstance]getModifiedContacts]count]>0) { 
        [_btnToggle setHidden:NO];
    }else{ 
        [_btnToggle setHidden:YES];
    }
    [_btnToggle setSelected:NO];
    [self performSelectorOnMainThread:@selector(btnToggleTap:) withObject:_btnToggle waitUntilDone:NO]; 
}




@end
