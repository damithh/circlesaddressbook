//
//  DHContactsTVC.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "DHContactsTVC.h"

@interface DHContactsTVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblChanges;
@property (weak, nonatomic) IBOutlet UITextView *txtPhoneNumbers;

@end

@implementation DHContactsTVC


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.txtPhoneNumbers setEditable:NO];
    }
    return self;
}

- (void)setContact:(DHContact *)contact{ 
    _lblName.text = contact.contactName;
    [_txtPhoneNumbers setText:contact.contactPhoneNumber];
    if (_cellType == DHContactCellTypeAll) {
        [_lblChanges setHidden:YES];
    }else{
        [_lblChanges setHidden:NO];
        UIColor *color = [UIColor greenColor];
        switch (contact.contactChangeMode.intValue) {
            case ContactChangeModeAdded:
                [_lblChanges setText:@"Added"];
                break;
            case ContactChangeModeDeleted:
                [_lblChanges setText:@"Removed"];
                color = [UIColor redColor];
                break;
            case ContactChangeModeUpdated:
                [_lblChanges setText:@"Changed"];
                color = [UIColor colorWithRed:66/255.0 green:169/255.0 blue:185/255.0 alpha:1.0f];
                break;
            default:
                break;
        }
        [_lblChanges setTextColor:color];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
