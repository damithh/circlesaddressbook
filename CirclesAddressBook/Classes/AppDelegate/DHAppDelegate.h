//
//  DHAppDelegate.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/7/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;

@end
