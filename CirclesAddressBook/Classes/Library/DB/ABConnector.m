//
//  ABConnector.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "ABConnector.h"
#import <AddressBook/AddressBook.h>
#import "DHActivityIndicator.h"

NSString *const SyncCompletedNotification = @"SyncCompletedNotification";

@implementation ABConnector

+ (instancetype)sharedInstance{
    static ABConnector *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (void)syncWithAddressBook{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
        //1
        NSLog(@"Denied");
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            if (!granted){
                //4
                UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                [cantAddContactAlert show];
                NSLog(@"Just denied");
                [[NSNotificationCenter defaultCenter]postNotificationName:SyncCompletedNotification object:nil];
                return;
            }
            //5
            NSLog(@"Just authorized");
        });
        NSLog(@"Not determined");
    }
    [[DHActivityIndicator sharedInstance]showActivityIndecator];
    [self performSelectorInBackground:@selector(updateDataBase) withObject:nil];
}

- (NSString *)removeNullValueFromStr:(NSString *)str{
    if (!str) {
        str = @"";
    }
    return str;
}

- (void)updateDataBase{
    [[DBManager sharedInstance]resetDB];
    NSArray *allLocalContacts = [[DBManager sharedInstance] getAllContacts];
    NSArray *recordIds = [allLocalContacts valueForKey:@"contactId"];
    NSMutableArray *containRecordIds = [[NSMutableArray alloc]initWithCapacity:[recordIds count]];
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    for(int i = 0; i < numberOfPeople; i++) {
        ABRecordRef person = CFArrayGetValueAtIndex( allPeople, i );
        NSNumber *recordID = [NSNumber numberWithInt:ABRecordGetRecordID(person)];
        NSString *firstName = [NSString stringWithFormat:@"%@ %@ %@",[self removeNullValueFromStr: (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty)],[self removeNullValueFromStr:(__bridge NSString *)ABRecordCopyValue(person, kABPersonMiddleNameProperty)], [self removeNullValueFromStr:(__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty)]];
        
        ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex j = ABMultiValueGetCount(phones);
        NSString *phoneNumber = @"";
        if (j>0) {
            for (int k = 0; k<j; k++) {
                NSString *pNumber = (__bridge NSString *)(ABMultiValueCopyValueAtIndex(phones, k));
                pNumber = [self removeNullValueFromStr:pNumber];
                if (k!=0) {
                    phoneNumber = [NSString stringWithFormat:@"%@\n%@",phoneNumber,pNumber];
                }else{
                    phoneNumber = [NSString stringWithFormat:@"%@",pNumber];
                }
            } 
        } 
        if ([recordIds containsObject:recordID]) {
            //            to update records
            DHContact *contact = [allLocalContacts objectAtIndex:[recordIds indexOfObject:recordID]];
            BOOL isChanged=NO;
            if (![contact.contactName isEqualToString:firstName]) {
                contact.contactName = firstName;
                isChanged = YES;
            }
            if (![contact.contactPhoneNumber isEqualToString:phoneNumber]) {
                contact.contactPhoneNumber = phoneNumber;
                isChanged = YES;
            }
            if (isChanged) {
                contact.contactChangeMode = [NSNumber numberWithInt:ContactChangeModeUpdated];
            }
            [containRecordIds addObject:recordID];
        }else{
            //            to inserted records
            DHContact *contact = (DHContact *)[NSEntityDescription insertNewObjectForEntityForName:@"DHContact" inManagedObjectContext:[(DHAppDelegate *)[[UIApplication sharedApplication]delegate]managedObjectContext]];
            [contact setContactId:recordID];
            [contact setContactName:firstName];
            [contact setContactPhoneNumber:phoneNumber];
            [contact setContactChangeMode:[NSNumber numberWithInt:ContactChangeModeAdded]];
        }
    }
    //    to deleted records
    NSMutableArray *nonContainRecordIds = [[NSMutableArray alloc]initWithArray:recordIds];
    [nonContainRecordIds removeObjectsInArray:containRecordIds];
    for (NSNumber *recordID in nonContainRecordIds) {
        DHContact *contact = [allLocalContacts objectAtIndex:[recordIds indexOfObject:recordID]];
        contact.contactChangeMode = [NSNumber numberWithInt:ContactChangeModeDeleted];
    } 
    [(DHAppDelegate *)[[UIApplication sharedApplication]delegate] saveContext];
    [self performSelectorOnMainThread:@selector(hideActivitiIndicator) withObject:nil waitUntilDone:NO];
    [[NSNotificationCenter defaultCenter]postNotificationName:SyncCompletedNotification object:nil];
}


- (void)hideActivitiIndicator{
    [[DHActivityIndicator sharedInstance]hideActivityIndecator];
}

@end
