//
//  DBManager.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "DBManager.h"
#import <CoreData/CoreData.h>


@implementation DBManager

+ (instancetype)sharedInstance{
    static DBManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (NSArray *)getAllContacts{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DHContact" inManagedObjectContext:[self manageObjectContext]];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contactChangeMode != %d", ContactChangeModeDeleted];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self manageObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        fetchedObjects = [[NSArray alloc]init];
    }
    return fetchedObjects;
}

- (NSArray *)getModifiedContacts{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DHContact" inManagedObjectContext:[self manageObjectContext]];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contactChangeMode != %d", ContactChangeModeNotChaged];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchedObjects = [[self manageObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        fetchedObjects = [[NSArray alloc]init];
    }
    return fetchedObjects;
}

- (void)resetDB{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DHContact" inManagedObjectContext:[self manageObjectContext]];
    [fetchRequest setEntity:entity]; 
    NSError *error = nil;
    NSArray *fetchedObjects = [[self manageObjectContext] executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        return;
    }
    for (DHContact *contact in  fetchedObjects) {
        if (contact.contactChangeMode.intValue == ContactChangeModeDeleted) {
            [[self manageObjectContext]deleteObject:contact];
        }else if(contact.contactChangeMode.intValue != ContactChangeModeNotChaged){
            contact.contactChangeMode = [NSNumber numberWithInt:ContactChangeModeNotChaged];
        }
    }
    [[self appDelegate]saveContext];
}

- (DHAppDelegate *)appDelegate{
    return [[UIApplication sharedApplication]delegate];;
}

- (NSManagedObjectContext *)manageObjectContext{
    return [[self appDelegate] managedObjectContext];
}


@end
