//
//  DBManager.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DHAppDelegate.h"
#import "DHContact.h"


@interface DBManager : NSObject

+ (instancetype)sharedInstance;

- (NSArray *)getAllContacts;
- (NSArray *)getModifiedContacts; 
- (void)resetDB;

@end

