//
//  ABConnector.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/9/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"

extern NSString *const SyncCompletedNotification; 


@interface ABConnector : NSObject

+ (instancetype)sharedInstance;
- (void)syncWithAddressBook; 

@end
