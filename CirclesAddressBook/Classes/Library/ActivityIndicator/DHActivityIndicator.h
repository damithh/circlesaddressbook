//
//  DHActivityIndicator.h
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/7/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHActivityIndicator : UIView

@property(nonatomic,weak)NSString *lodingStr;

+ (instancetype)sharedInstance;
- (void)showActivityIndecator;
- (void)hideActivityIndecator;


@end
