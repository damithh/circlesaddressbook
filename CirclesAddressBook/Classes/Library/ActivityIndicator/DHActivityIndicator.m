//
//  DHActivityIndicator.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/7/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import "DHActivityIndicator.h"
#import <QuartzCore/QuartzCore.h>

@interface DHActivityIndicator ()

@property(nonatomic, strong)UIView *backgroudView;
@property(nonatomic, strong)UIActivityIndicatorView *activityIndecatorView;
@property(nonatomic, strong)UILabel *lblTitle;

@end


@implementation DHActivityIndicator

+ (instancetype)sharedInstance{
    static DHActivityIndicator *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}


- (id)init{
    self = [super init];
    if (self) {
        self.backgroudView = [[UIView alloc]init];
        [_backgroudView.layer setCornerRadius:10.0f];
        self.activityIndecatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.lblTitle = [[UILabel alloc]init];
        [_lblTitle setBackgroundColor:[UIColor clearColor]];
        [_lblTitle setTextColor:[UIColor lightGrayColor]];
        [_lblTitle setTextAlignment:NSTextAlignmentCenter];
        [_lblTitle setText:@"Syncing..."];
        [_lblTitle setFont:[_lblTitle.font fontWithSize:18.0f]];
        _lblTitle.adjustsFontSizeToFitWidth = YES; 
        [_backgroudView addSubview:_activityIndecatorView];
        [_backgroudView addSubview:_lblTitle];
        [self setBackgroundColor:[[UIColor blackColor]colorWithAlphaComponent:.8]];
        [_backgroudView setBackgroundColor:[UIColor blackColor]];
        [self addSubview:_backgroudView];
        [self addAutoLaout];
    } 
    return self;
}


- (void)addAutoLaout{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_backgroudView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_activityIndecatorView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSDictionary *views = NSDictionaryOfVariableBindings(_backgroudView, _activityIndecatorView, _lblTitle);
    NSDictionary * metrics = @{@"backgroundVHeight":@([[UIScreen mainScreen]bounds].size.width/4)};
//    backgroundView
    [_backgroudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_backgroudView(backgroundVHeight)]"options:0 metrics:metrics views:views]];
    [_backgroudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_backgroudView(backgroundVHeight)]"options:0 metrics:metrics views:views]]; 
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_backgroudView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_backgroudView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
//    _activityIndecatorView
    [_backgroudView addConstraint:[NSLayoutConstraint constraintWithItem:_activityIndecatorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_backgroudView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [_backgroudView addConstraint:[NSLayoutConstraint constraintWithItem:_activityIndecatorView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_backgroudView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-5.0f]];
//    _lblTitle
    [_backgroudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_lblTitle(20)]-5-|"options:0 metrics:metrics views:views]];
    [_backgroudView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[_lblTitle]-5-|"options:0 metrics:metrics views:views]];
}

- (void)showActivityIndecator{
    [_activityIndecatorView startAnimating];
    UIView *superView = [[[UIApplication sharedApplication]delegate]window];
    [superView addSubview:self];
    UIView *view = self;
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSDictionary * views = NSDictionaryOfVariableBindings(view);
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|"options:0 metrics:nil views:views]];
    [superView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|"options:0 metrics:nil views:views]];
}

- (void)hideActivityIndecator{
    [_activityIndecatorView stopAnimating];
    [self removeFromSuperview];
}

@end
