//
//  main.m
//  CirclesAddressBook
//
//  Created by Damith Hettige on 3/7/15.
//  Copyright (c) 2015 D D C Hettige. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DHAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DHAppDelegate class]));
    }
}
